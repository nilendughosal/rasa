#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 18 00:00:31 2021

@author: nilendughosal
"""


import requests

# Set the request parameters
url = 'https://dev112354.service-now.com/api/now/table/incident?sysparm_limit=1'

# Eg. User name="admin", Password="admin" for this code sample.
user = 'admin'
pwd = 'Nd5wgC3rjAEZ'

# Set proper headers
headers = {"Content-Type":"application/json","Accept":"application/json"}


'''
# Do the HTTP request
response = requests.get(url, auth=(user, pwd), headers=headers )

# Check for HTTP codes other than 200
if response.status_code != 200: 
    print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
    exit()

# Decode the JSON response into a dictionary and use the data
data = response.json()
print(data)
'''


url = 'https://dev112354.service-now.com/api/now/table/incident'
# Do the HTTP request
response = requests.post(url, auth=(user, pwd), headers=headers ,data="{\"short_description\":\"Test RASA\",\"description\":\"Test RASA\"}")

# Check for HTTP codes other than 200
if response.status_code != 201: 
    print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
    exit()

# Decode the JSON response into a dictionary and use the data
data = response.json()
print(data)