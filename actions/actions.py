# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

from rasa_sdk.events import SlotSet


import yaml
from SnowUtils import SnowUtility as sutil

with open("/Users/nilendughosal/Desktop/RASA/DemoBot3/SnowUtils/snow_config.yml", "r") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

host = cfg["SERVICE_NOW"]["HOST"]
username = cfg["SERVICE_NOW"]["USERNAME"]
password = cfg["SERVICE_NOW"]["PASSWORD"]



class ActionHelloWorld(Action):
    def name(self) -> Text:
        return "action_hello_world"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        dispatcher.utter_message(text="Action Server Test Sucess")
        return []

class ActionShowIncident(Action):
    def name(self) -> Text:
        return "action_show_incident"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        su = sutil.SnowUtility(host , username , password)
        data = su.getIncident()
        dispatcher.utter_message(text="SNOW CONNECTION TEST \n" + data)
        return []



'''
GET INDIENT BY NUMBER
'''

class ActionGetIncidentByNumber(Action):
    def name(self) -> Text:
        return "action_get_incident_by_number"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        su = sutil.SnowUtility(host , username , password)
        incident_number = tracker.get_slot("incident_number")
        data = su.getIncidentByNumber(incident_number)
        dispatcher.utter_message(text="Incident Details: \n" + data)
        return []


class ActionResetIncidentNumber(Action):
    def name(self):
        return "action_reset_incident_number"

    def run(self, dispatcher, tracker, domain):
        return [SlotSet("incident_number", None)]




'''
RAISE INCIDENT
'''

class ActionRaiseIncident(Action):
    def name(self) -> Text:
        return "action_raise_incident"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        su = sutil.SnowUtility(host , username , password)
        short_description = tracker.get_slot("short_description")
        description = tracker.get_slot("description")
        data = su.raiseIncident(short_description , description)
        dispatcher.utter_message(text="Incident Number: " + data)
        return []


class ActionResetRaiseIncidentSlots(Action):
    def name(self):
        return "action_reset_raise_incident_slots"

    def run(self, dispatcher, tracker, domain):
        return [SlotSet("short_description", None) , SlotSet("description", None)]
    
    