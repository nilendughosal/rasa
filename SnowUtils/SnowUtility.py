#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 18 00:18:34 2021

@author: nilendughosal
"""

import requests

class SnowUtility:
    
    
    def __init__(self , host , username , password):
        self.host = host
        self.username = username
        self.password = password
        
    
    def getIncident(self):
        
        url = self.host + "/api/now/table/incident?sysparm_limit=1&sysparm_fields=number,assignment_group.name,state"
        headers = {"Content-Type":"application/json","Accept":"application/json"}
        
        response = requests.get(url, auth=(self.username, self.password), headers=headers )
        
        
        # Check for HTTP codes other than 200
        if response.status_code != 200: 
            print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
            exit()

        # Decode the JSON response into a dictionary and use the data
        data = response.json()
        #print(data)
        return(data['result'][0]['number'] + "\n" + data['result'][0]['assignment_group.name'] + "\n" + data['result'][0]['state'])
        pass
    
    
    def getIncidentByNumber(self , incidentNumber):
        
        url = self.host + "/api/now/table/incident?sysparm_query=number={}&sysparm_fields=number,assignment_group.name,state,short_description".format(incidentNumber)
        headers = {"Content-Type":"application/json","Accept":"application/json"}
        
        response = requests.get(url, auth=(self.username, self.password), headers=headers )
        
        
        # Check for HTTP codes other than 200
        if response.status_code != 200: 
            print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
            exit()

        # Decode the JSON response into a dictionary and use the data
        data = response.json()
        #print(data)
        return(data['result'][0]['number'] + "\n" + data['result'][0]['assignment_group.name'] + "\n" + data['result'][0]['short_description'])
        pass
    
    
    def raiseIncident(self , short_description , description):
        url = 'https://dev112354.service-now.com/api/now/table/incident'
        headers = {"Content-Type":"application/json","Accept":"application/json"}
        
        # Do the HTTP request
        #payload = "{\"short_description\":\"Test RASA\",\"description\":\"Test RASA\"}"
        payload = '{"short_description":' + f'"{short_description}"' + ',"description":' + f'"{description}"' +  '}'
        response = requests.post(url, auth=(self.username, self.password), headers=headers ,data=payload)

        # Check for HTTP codes other than 200
        if response.status_code != 201: 
            print('Status:', response.status_code, 'Headers:', response.headers, 'Error Response:',response.json())
            exit()

        # Decode the JSON response into a dictionary and use the data
        data = response.json()
        return(data['result']['number'])
    
